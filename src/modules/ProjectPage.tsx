interface Props{
    projects,
    refreshData:()=>void
}

const ProjectPage = ( {projects, refreshData}:Props ) => {

    const { register, handleSubmit, reset, formState: {errors} } = useForm();

    const onSubmit = async (data) => {
        try {
            const body = { name: data.ProjectName }

            await fetch(
                '/api/projects',
                {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: {JSON.stringify(body)}
                }
            );
            refreshData();
            reset();
        } catch (error) {
            console.log(error)
        }
    }

  return (
    <div>

    <ul>
        (projects && projects.map( (project, index) => (
            <li className="w-full justify-between mb-5" key={index} >
                <Project project={project} refreshData={refreshData} />
            </li>
        ) ))
    </ul>

    </div>
  )
}

export default ProjectPage