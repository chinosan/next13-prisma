import prisma from "@/lib/prisma";
import { GetServerSideProps } from "next";

export const getServerSideProps: GetServerSideProps = async ({params}) {

    if(!params?.id) {
        return {
            props: []
        }
    }
    const project = await prisma.projects.findUnique({
    
        where: {
            id: Number(params.id)
        },
        include: {
            Tasks: true
        }
    })

    return {
        props: {projects}
    }
}
