import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from './page.module.css'
import { GetServerSideProps } from 'next'
import prisma from '@/lib/prisma'
import { Prisma } from '@prisma/client'
import { useRouter } from 'next/router'
import ProjectPage from '@/modules/ProjectPage'

const inter = Inter({ subsets: ['latin'] })

interface Props{

  projects: Prisma.ProjectsSelect[];

}

export default function Home( { projects }: Props ) {

  const router = useRouter();

  const refreshData = () => {

    router.replace(router.asPath)

  }

  return (
    <main className={styles.main}>
      
      <ProjectPage projects={projects} refreshData={refreshData} />

    </main>
  )
}

export const getServerSideProps: GetServerSideProps = async () => {

  const projects = await prisma.projects.findMany();
  return {
    props: { projects }
  }
}
